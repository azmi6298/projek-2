$(document).ready(function() {

    //insert text when enter key pressed
    $('#inputtxt').keypress(function(e) {
        var text = $('#inputtxt').val();
        var trimmed = text.trim();
        var newdiv = $('<div class="msg-send"> <p>' + text + '</p></div> <div class="msg-receive"> <p> Message received</p></div>');
        if (e.which == 13) {
            if (trimmed != "") {
                $('.chat-body').append(newdiv);
                $('#inputtxt').val('');
            }
        }
    });

    //toggle chat window when arrow image pressed 
    //change arrow image (up&down) when toggling chat body
    var count = 0;
    $('#toggle').click(function() {
        count += 1;

        if (count % 2 != 0) {
            $('#toggle').attr('src', 'https://img.icons8.com/windows/32/000000/collapse-arrow.png');
        } else {
            $('#toggle').attr('src', 'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png');
        }
        $('.chat-body').toggle();
    });

    //list of themes
    var themes = [{ "id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA" },
        { "id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA" },
        { "id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA" },
        { "id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA" },
        { "id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121" },
        { "id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121" },
        { "id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121" },
        { "id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121" },
        { "id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121" },
        { "id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121" },
        { "id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA" }
    ];

    var selectedTheme = { "Indigo": { "bcgColor": "#3F51B5", "fontColor": "#FAFAFA" } };

    localStorage.setItem('themes', JSON.stringify(themes));
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));

    var getThemes = localStorage.getItem('themes');
    var getSelectedTheme = localStorage.getItem('selectedTheme');

    console.log(JSON.parse(getThemes));
    console.log(JSON.parse(getSelectedTheme));


    //insert data into select list
    var selected = $('.my-select').val('0');
    $('.my-select').select2({
        data: themes
    });

    $('.apply-button').on('click', function() {
        var select = $('.my-select').val(); //get value from selected theme


        if (!themes.indexOf(select)) {
            console.log('Theme not found');
        } else {
            var obj = $('.my-select').select2("data");
            $('body').css("background-color", obj[0].bcgColor);
            $('h1').css("color", obj[0].fontColor);
            var changedColor = $('<div class="container text-center"><h3>Theme changed to ' + obj[0].text + '</h3></div>');
            $('body').append(changedColor);
            console.log(obj[0].fontColor);
        }
    });



});