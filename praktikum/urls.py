"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1
from my_experience.views import myexp
from my_profile.views import myprof
from my_portfolio.views import myport
from lab_4.views import chatbox
# import lab_8.urls as lab_8
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import LoginView, LogoutView


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^experience/', myexp, name='experience'),
    re_path(r'^profile/', myprof, name='profile'),
    re_path(r'^portfolio/', myport, name='portfolio'),
    re_path(r'^chatbox/', chatbox, name='chatbox'),
    re_path(r'^$', index_lab1, name='index'),
    # re_path(r'^lab-8/', include(lab_8, namespace='lab_8')),
    re_path(r'login/', LoginView.as_view(), name='login'),
    re_path(r'logout/', LogoutView.as_view(), name='logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
